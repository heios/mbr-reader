#include <array>
#include <cstdint>
#include <iostream>
#include <fstream>

#include "tools.hpp"
#include "master-boot-record.hpp"


void print_help(char const * const first_arg) {
  std::cerr << first_arg << " <path-to-file-with-MBR>\n";
}


int main(int ac, char* av[]) {
  if (ac < 2) {
    print_help(av[0]);
    return 1;
  }

  std::ifstream file_stream(av[1], std::ios::binary);
  if (!file_stream) {
    std::cerr << "Can't open file: \"" << av[1] << "\"\n";
    return 2;
  }

  file_stream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  if (file_stream) {
    std::array<char, 512> buf;
    file_stream.read(buf.data(), buf.size());
    const auto bytes_read = file_stream.gcount();
    if (bytes_read != buf.size()) {
      std::cerr << "Read " << bytes_read << " bytes instead of " << buf.size() << "." << std::endl;
      return 4;
    }

    const auto& [data, error] = ::mbr::read_data(buf.data(), std::next(buf.data(), buf.size()));
    if (error.has_value()) {
      std::cerr << "Error while parsing MBR: " << ::mbr::to_string(error.value()) << std::endl;
      std::cerr << "end_of_sector: " << escape_nonascii(
        std::next(buf.data(), buf.size() - 2),
        std::next(buf.data(), buf.size())) << std::endl;
      return 3;
    }

    std::cout << to_string(data) << std::endl;

    // std::cout << "disk_id: " << escape_nonascii(
    //   data.disk_id.data(), std::next(data.disk_id.data(), data.disk_id.size())
    // );
    std::cout << std::endl;
  }


  return 0;
}
