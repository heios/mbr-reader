#include "master-boot-record.hpp"

#include <tuple>
#include <sstream>

#include "tools.hpp"


namespace disk_addr
{

auto to_string(const CylinderHeadSector24 chs) -> std::string
{
  return "{cylinder: " + std::to_string(chs.cylinder)
    + ", head: " + std::to_string(chs.head)
    + ", sector: " + std::to_string(chs.sector)
    + "}";
}

}

namespace mbr
{

namespace utils
{

template<
  class Pointer,
  class DataType = std::remove_pointer_t<Pointer>,
  class SFINAE = std::enable_if_t<
    std::is_pointer_v<Pointer>
    and std::is_pod_v<DataType>
    and std::is_const_v<DataType>
  >
>
char const * cast_to_char_ptr(Pointer&& ptr)
{
  return static_cast<char const *>(static_cast<void const *>(ptr));
}

template<
  class Pointer,
  class DataType = std::remove_pointer_t<Pointer>,
  class SFINAE = std::enable_if_t<
    std::is_pointer_v<Pointer>
    and std::is_pod_v<DataType>
    and not std::is_const_v<DataType>
  >
>
char* cast_to_char_ptr(Pointer&& ptr)
{
  return static_cast<char*>(static_cast<void*>(ptr));
}

}  // namespace utils


auto read_data(char const * const first, char const * const last)
      -> std::pair<Data, std::optional<ReadDataError>>
{
  auto result = Data{};

  if (std::distance(first, last) != 512) {
    return {result, ReadDataError::bad_data_block_size};
  }

  const auto after_boot_loader = std::next(first, result.bootloader_code.size());
  std::copy(
    first, after_boot_loader,
    ::mbr::utils::cast_to_char_ptr(result.bootloader_code.data())
  );

  const auto after_disk_id = std::next(after_boot_loader, result.disk_id.size());
  std::copy(
    after_boot_loader, after_disk_id,
    ::mbr::utils::cast_to_char_ptr(result.disk_id.data())
  );

  const auto after_reserved_block = std::next(after_disk_id, result.reserved_block.size());
  std::copy(
    after_disk_id, after_reserved_block,
    ::mbr::utils::cast_to_char_ptr(result.reserved_block.data())
  );

  constexpr auto partition_table_raw_size = 16;
  auto bytes_ptr = after_reserved_block;
  for (auto &partition_table : result.partition_tables) {
    bool status = false;
    std::tie(partition_table, status) = read_partition_table(
      bytes_ptr, std::next(bytes_ptr, partition_table_raw_size)
    );
    if (!status) {
      return {result, ReadDataError::bad_partition_table_data};
    }
    std::advance(bytes_ptr, partition_table_raw_size);
  }

  const auto after_partion_tables = bytes_ptr;
  const auto end_of_sector_correct = std::equal(
    after_partion_tables,
    std::next(after_partion_tables, 2),
    ::mbr::utils::cast_to_char_ptr(::mbr::Data::end_of_sector.data())
  );

  if (not end_of_sector_correct) {
    return {result, ReadDataError::bad_end_of_sector};
  }

  return {result, std::nullopt};
}


auto read_partition_table(char const * const first, char const * const last)
    -> std::pair<PartitionTable, bool>
{
  auto result = mbr::PartitionTable{};

  if (std::distance(first, last) != 16) {
    return {result, false};
  }

  const auto boot_indicator_ptr = first;
  result.boot_indicator = static_cast<mbr::BootIndicator>(*boot_indicator_ptr);
  switch (result.boot_indicator) {
    case mbr::BootIndicator::not_bootable:
    case mbr::BootIndicator::bootable:
      break;
    default:
      return {result, false};
  }

  constexpr auto read_chs = [](char const * const raw_chs) {
    ::disk_addr::CylinderHeadSector24 chs;
    decltype(auto) data = static_cast<uint8_t const * const>(
      static_cast<void const * const>(raw_chs)
    );
    chs.head = data[0];
    chs.sector = (data[1] << 2) >> 2;
    chs.cylinder = ((static_cast<int>(data[1]) >> 6) << 8) | data[2];
    return chs;
  };

  constexpr auto boot_indicator_size = sizeof(uint8_t);
  const auto chs_first_sector_ptr = std::next(boot_indicator_ptr, boot_indicator_size);
  result.chs_span.first_sector = read_chs(chs_first_sector_ptr);

  constexpr auto chs_raw_size = 3;
  const auto partition_type_ptr = std::next(chs_first_sector_ptr, chs_raw_size);
  result.partition_type = static_cast<mbr::PartitionType>(*partition_type_ptr);

  constexpr auto partition_type_size = sizeof(uint8_t);
  const auto chs_last_sector_ptr = std::next(partition_type_ptr, partition_type_size);
  result.chs_span.last_sector_inclusive = read_chs(chs_last_sector_ptr);

  // TODO(heios): Make code that works for any endianess.
  constexpr auto lba_sector_offset_size = sizeof(uint32_t);
  const auto lba_first_sector_ptr = std::next(chs_last_sector_ptr, chs_raw_size);
  std::copy(
    lba_first_sector_ptr,
    std::next(lba_first_sector_ptr, lba_sector_offset_size),
    ::mbr::utils::cast_to_char_ptr(&result.lba_span.first_sector)
  );

  constexpr auto lba_number_of_sectors_size = sizeof(uint32_t);
  const auto lba_number_of_sectors_ptr = std::next(lba_first_sector_ptr, lba_sector_offset_size);
  std::copy(
    lba_number_of_sectors_ptr,
    std::next(lba_number_of_sectors_ptr, lba_number_of_sectors_size),
    ::mbr::utils::cast_to_char_ptr(&result.lba_span.number_of_sectors)
  );

  return {result, true};
}


auto to_string(const PartitionTable partition_table)
    -> std::string
{
  std::ostringstream out_stream;
  out_stream
    << "partition_table:"
    << "\n  boot_indicator: " << to_string(partition_table.boot_indicator)
    << "\n  partition_type: " << to_string(partition_table.partition_type)
    << "\n  CylinderHeadSector_span:"
    << "\n    first_sector: "
      << to_string(partition_table.chs_span.first_sector)
    << "\n    last_sector_inclusive: "
      << to_string(partition_table.chs_span.last_sector_inclusive)
    << "\n  LogicalBlockAddressing_span:"
    << "\n    first_sector_offset: "
      << separate_thousands(std::to_string(partition_table.lba_span.first_sector))
    << "\n    last_sector_offset_exclusive: "
      << separate_thousands(std::to_string(
          partition_table.lba_span.first_sector + partition_table.lba_span.number_of_sectors
         ))
    << "\n    number_of_sectors: "
      << separate_thousands(std::to_string(partition_table.lba_span.number_of_sectors))
    << "\n    bytes_format:"
    << "\n      first_sector_offset: "
      << separate_thousands(std::to_string(
          partition_table.lba_span.first_sector * ::mbr::bytes_in_sector
        ))
    << "\n      last_sector_offset_exclusive: "
      << separate_thousands(std::to_string(
          (partition_table.lba_span.first_sector + partition_table.lba_span.number_of_sectors)
            * ::mbr::bytes_in_sector
         ))
    << "\n      number_of_bytes: "
      << separate_thousands(std::to_string(
          partition_table.lba_span.number_of_sectors * ::mbr::bytes_in_sector
         ))
  ;
  return out_stream.str();
}

auto to_string(const Data& data)
    -> std::string
{
  std::ostringstream out_stream;
  out_stream
    << "MBR:"
    << "\n  disk_id: " << replace_all(hexdump(data.disk_id), " ", "")
    << "\n  bootloader: >-\n    " << replace_all(hexdump(data.bootloader_code), "\n", "\n    ")
    << "\n  reserved_block: " << hexdump(data.reserved_block)
  ;

  out_stream << "\n  partition_tables:";
  for (const auto& partition_table : data.partition_tables) {
    out_stream
      << "\n    -\n    "
      << replace_all(to_string(partition_table), "\n", "\n    ")
      ;
  }
  return out_stream.str();
}

auto to_string(const ReadDataError read_data_error)
    -> std::string
{
  switch (read_data_error) {
    case ::mbr::ReadDataError::success: return "success";
    case ::mbr::ReadDataError::bad_data_block_size: return "bad_data_block_size";
    case ::mbr::ReadDataError::bad_partition_table_data: return "bad_partition_table_data";
    case ::mbr::ReadDataError::bad_end_of_sector: return "bad_end_of_sector";
  }
  std::abort();
}

auto to_string(const BootIndicator boot_indicator)
    -> std::string
{
  switch (boot_indicator) {
    case BootIndicator::not_bootable: return "not_bootable";
    case BootIndicator::bootable: return "bootable";
  }
  ::std::abort();
}

auto to_string(const PartitionType partition_type)
    -> std::string
{
  switch (partition_type) {
    case PartitionType::empty: return "empty";
    case PartitionType::fat12_primary_or_logical_elsewhere:
      return "fat12_primary_or_logical_elsewhere";
    case PartitionType::xenix_root: return "xenix_root";
    case PartitionType::xenix_usr: return "xenix_usr";
    case PartitionType::fat16: return "fat16";
    case PartitionType::chs_8g: return "chs_8g";
    case PartitionType::fat16b: return "fat16b";
    case PartitionType::ntfs_or_exfat: return "ntfs_or_exfat";
    case PartitionType::logical_sectored_fat: return "logical_sectored_fat";
    case PartitionType::aix_qnx_os9: return "aix_qnx_os9";
    case PartitionType::os2_boot: return "os2_boot";
    case PartitionType::fat32_with_chs: return "fat32_with_chs";
    case PartitionType::fat32_with_lba: return "fat32_with_lba";
    case PartitionType::fat16b_with_lba: return "fat16b_with_lba";
    case PartitionType::extended_with_lba: return "extended_with_lba";
    case PartitionType::unisys_opus: return "unisys_opus";
    case PartitionType::logical_or_hidden_fat12: return "logical_or_hidden_fat12";
    case PartitionType::hibernation_or_service_or_rescue: return "hibernation_or_service_or_rescue";
    case PartitionType::logical_or_hidden_fat16: return "logical_or_hidden_fat16";
    case PartitionType::hidden_extended_with_chs: return "hidden_extended_with_chs";
    case PartitionType::hidden_fat16b: return "hidden_fat16b";
    case PartitionType::hidden_ntfs_or_exfat: return "hidden_ntfs_or_exfat";
    case PartitionType::ast_zero_suspended: return "ast_zero_suspended";
    case PartitionType::willowtech_photon_cos: return "willowtech_photon_cos";
    case PartitionType::hidden_fat32: return "hidden_fat32";
    case PartitionType::hidden_fat32_with_lba: return "hidden_fat32_with_lba";
    case PartitionType::hidden_fat16_with_lba: return "hidden_fat16_with_lba";
    case PartitionType::hidden_extended_with_lba: return "hidden_extended_with_lba";
    case PartitionType::willowsoft_overture_fs1: return "willowsoft_overture_fs1";
    case PartitionType::oxygen_file_system_fso2: return "oxygen_file_system_fso2";
    case PartitionType::oxygen_extended_partition_table: return "oxygen_extended_partition_table";
    case PartitionType::windows_mobile_boot_xip: return "windows_mobile_boot_xip";
    case PartitionType::logical_sectored_fat12_or_fat16: return "logical_sectored_fat12_or_fat16";
    case PartitionType::windows_mobile_imgfs: return "windows_mobile_imgfs";
    case PartitionType::microsoft_ibm_reserved_code26: return "microsoft_ibm_reserved_code26";
    case PartitionType::hidden_ntfs_windows_recovery_env: return "hidden_ntfs_windows_recovery_env";
    case PartitionType::anthe_os_filesystem: return "anthe_os_filesystem";
    case PartitionType::syllable_secure: return "syllable_secure";
    case PartitionType::microsoft_ibm_reserved_code31: return "microsoft_ibm_reserved_code31";
    case PartitionType::alien_internet_services_nos: return "alien_internet_services_nos";
    case PartitionType::microsoft_ibm_reserved_code33: return "microsoft_ibm_reserved_code33";
    case PartitionType::microsoft_ibm_reserved_code34: return "microsoft_ibm_reserved_code34";
    case PartitionType::jfs: return "jfs";
    case PartitionType::microsoft_ibm_reserved_code36: return "microsoft_ibm_reserved_code36";
    case PartitionType::theos_v3_2: return "theos_v3_2";
    case PartitionType::plan9_edition3_partition: return "plan9_edition3_partition";
    case PartitionType::theos_v4: return "theos_v4";
    case PartitionType::theos_v4_extended: return "theos_v4_extended";
    case PartitionType::partition_magic_in_progress: return "partition_magic_in_progress";
    case PartitionType::hidden_netware: return "hidden_netware";
    case PartitionType::pick_r83_or_venix_80286: return "pick_r83_or_venix_80286";
    case PartitionType::risc_or_old_linux_or_powerpc: return "risc_or_old_linux_or_powerpc";
    case PartitionType::sfs_or_old_linux_swap_or_microsoft_dynamic_extended:
      return "sfs_or_old_linux_swap_or_microsoft_dynamic_extended";
    case PartitionType::old_linux_native: return "old_linux_native";
    case PartitionType::goback_softwares: return "goback_softwares";
    case PartitionType::priam_or_boot_us_manager: return "priam_or_boot_us_manager";
    case PartitionType::eumel_elan_x46: return "eumel_elan_x46";
    case PartitionType::eumel_elan_x47: return "eumel_elan_x47";
    case PartitionType::eumel_elan_or_ergos_l3: return "eumel_elan_or_ergos_l3";
    case PartitionType::aquila_or_alfs: return "aquila_or_alfs";
    case PartitionType::aos_a2: return "aos_a2";
    case PartitionType::primary_qnx_posix: return "primary_qnx_posix";
    case PartitionType::secondary_qnx_posix: return "secondary_qnx_posix";
    case PartitionType::tertiary_qnx_posix_or_oberon: return "tertiary_qnx_posix_or_oberon";
    case PartitionType::lynx_rtos_or_oberon: return "lynx_rtos_or_oberon";
    case PartitionType::novell_or_on_track_aux1: return "novell_or_on_track_aux1";
    case PartitionType::cp_m_80: return "cp_m_80";
    case PartitionType::on_track_aux3: return "on_track_aux3";
    case PartitionType::on_track_ddo: return "on_track_ddo";
    case PartitionType::ez_drive: return "ez_drive";
    case PartitionType::logical_sectored_fat12_or_fat16_2:
      return "logical_sectored_fat12_or_fat16_2";
    case PartitionType::drivepro_vndi: return "drivepro_vndi";
    case PartitionType::priam_edisk_partitioned: return "priam_edisk_partitioned";
    case PartitionType::hidden_fat12: return "hidden_fat12";
    case PartitionType::unix_chs_or_hidden_fat12: return "unix_chs_or_hidden_fat12";
    case PartitionType::netware_fs286_2_or_hidden_fat16: return "netware_fs286_2_or_hidden_fat16";
    case PartitionType::netware_fs386: return "netware_fs386";
    case PartitionType::netware_fs386_or_hidden_fat16: return "netware_fs386_or_hidden_fat16";
    case PartitionType::netware_wolf_mountain: return "netware_wolf_mountain";
    case PartitionType::netware_code68: return "netware_code68";
    case PartitionType::netware_code69: return "netware_code69";
    case PartitionType::dragonfly_bsd_slice: return "dragonfly_bsd_slice";
    case PartitionType::dick_secure_multiboot: return "dick_secure_multiboot";
    case PartitionType::microsoft_ibm_reserved_code71: return "microsoft_ibm_reserved_code71";
    case PartitionType::apti_alternative_fat12: return "apti_alternative_fat12";
    case PartitionType::microsoft_ibm_reserved_code73: return "microsoft_ibm_reserved_code73";
    case PartitionType::hidden_fat16b_2: return "hidden_fat16b_2";
    case PartitionType::ibm_pc_ix: return "ibm_pc_ix";
    case PartitionType::hidden_read_only_fat16b: return "hidden_read_only_fat16b";
    case PartitionType::vndi_m2fs_m2cs: return "vndi_m2fs_m2cs";
    case PartitionType::xosl_bootloader: return "xosl_bootloader";
    case PartitionType::apti_alternative_fat16_chs: return "apti_alternative_fat16_chs";
    case PartitionType::apti_alternative_fat16_lba: return "apti_alternative_fat16_lba";
    case PartitionType::apti_alternative_fat16b_chs: return "apti_alternative_fat16b_chs";
    case PartitionType::apti_alternative_fat32_lba: return "apti_alternative_fat32_lba";
    case PartitionType::apti_alternative_fat32_chs: return "apti_alternative_fat32_chs";
    case PartitionType::primo_cache_level2: return "primo_cache_level2";
    case PartitionType::reserved_for_local_expirements: return "reserved_for_local_expirements";
    case PartitionType::minix_fs_old: return "minix_fs_old";
    case PartitionType::minix_fs: return "minix_fs";
    case PartitionType::linux_swap_or_solaris_x86: return "linux_swap_or_solaris_x86";
    case PartitionType::native_linux_fs: return "native_linux_fs";
    case PartitionType::hibernation_or_hidden_fat16: return "hibernation_or_hidden_fat16";
    case PartitionType::linux_extented: return "linux_extented";
    case PartitionType::linux_old_raid_superblock_or_fat16b_mirrored:
      return "linux_old_raid_superblock_or_fat16b_mirrored";
    case PartitionType::mirrored_hpfs_ntfs: return "mirrored_hpfs_ntfs";
    case PartitionType::linux_plaintext_partition_table: return "linux_plaintext_partition_table";
    case PartitionType::linux_kernel_image: return "linux_kernel_image";
    case PartitionType::mirrored_fat32: return "mirrored_fat32";
    case PartitionType::legacy_mirrored_fat32: return "legacy_mirrored_fat32";
    case PartitionType::hidden_fat12_code8d: return "hidden_fat12_code8d";
    case PartitionType::linux_lvm: return "linux_lvm";
    case PartitionType::hidden_fat16: return "hidden_fat16";
    case PartitionType::hidden_extended_with_chs_code91: return "hidden_extended_with_chs_code91";
    case PartitionType::hidden_fat16b_3: return "hidden_fat16b_3";
    case PartitionType::hidden_linux_or_amoeba_native: return "hidden_linux_or_amoeba_native";
    case PartitionType::amoeba_back_block_table: return "amoeba_back_block_table";
    case PartitionType::exopc_native: return "exopc_native";
    case PartitionType::iso9660: return "iso9660";
    case PartitionType::hidden_fat32_code97: return "hidden_fat32_code97";
    case PartitionType::hidden_fat32_or_service_partition:
      return "hidden_fat32_or_service_partition";
    case PartitionType::early_unix: return "early_unix";
    case PartitionType::hidden_fat16_2: return "hidden_fat16_2";
    case PartitionType::hidden_extended_with_lba_2: return "hidden_extended_with_lba_2";
    case PartitionType::forthos: return "forthos";
    case PartitionType::bsd_os_3_0_plus: return "bsd_os_3_0_plus";
    case PartitionType::hp_diagnostic_or_hibernate: return "hp_diagnostic_or_hibernate";
    case PartitionType::hp_expansion_or_hibernate: return "hp_expansion_or_hibernate";
    case PartitionType::altera_hps_arm_preloader: return "altera_hps_arm_preloader";
    case PartitionType::hp_expansion: return "hp_expansion";
    case PartitionType::hp_expansion_2: return "hp_expansion_2";
    case PartitionType::bsd_slice: return "bsd_slice";
    case PartitionType::openbsd_slice: return "openbsd_slice";
    case PartitionType::nextstep: return "nextstep";
    case PartitionType::mac_osx_ufs: return "mac_osx_ufs";
    case PartitionType::netbsd_slice: return "netbsd_slice";
    case PartitionType::msdos_fat12: return "msdos_fat12";
    case PartitionType::mac_osx_boot: return "mac_osx_boot";
    case PartitionType::max_osx_raid: return "max_osx_raid";
    case PartitionType::risc_os_adfs: return "risc_os_adfs";
    case PartitionType::shagos: return "shagos";
    case PartitionType::hfs_maybe_plus: return "hfs_maybe_plus";
    case PartitionType::star_tools_boot: return "star_tools_boot";
    case PartitionType::qnx_power_safe: return "qnx_power_safe";
    case PartitionType::qnx_power_safe2: return "qnx_power_safe2";
    case PartitionType::qnx_power_safe3: return "qnx_power_safe3";
    case PartitionType::hp_expansion_3: return "hp_expansion_3";
    case PartitionType::hp_expansion_or_corrupted_fat16b:
      return "hp_expansion_or_corrupted_fat16b";
    case PartitionType::corrupted_hpfs_ntfs_mirrored_master: return "corrupted_hpfs_ntfs_mirrored_master";
    case PartitionType::bsdi_swap_or_native: return "bsdi_swap_or_native";
    case PartitionType::corrupted_fat32_mirrored_master: return "corrupted_fat32_mirrored_master";
    case PartitionType::corrupted_fat32_mirrored_master_or_backup:
      return "corrupted_fat32_mirrored_master_or_backup";
    case PartitionType::bonny_dos_286: return "bonny_dos_286";
    case PartitionType::solaris8_boot: return "solaris8_boot";
    case PartitionType::solaris_x86: return "solaris_x86";
    case PartitionType::secured_fat: return "secured_fat";
    case PartitionType::secured_fat12: return "secured_fat12";
    case PartitionType::hidden_linux_native: return "hidden_linux_native";
    case PartitionType::hidden_linux_swap: return "hidden_linux_swap";
    case PartitionType::secured_fat16: return "secured_fat16";
    case PartitionType::secured_extented_partition_with_chs:
      return "secured_extented_partition_with_chs";
    case PartitionType::corrupted_fat16b_mirrored_slave: return "corrupted_fat16b_mirrored_slave";
    case PartitionType::corrupted_hpfs_ntfs_mirrored_slave:
      return "corrupted_hpfs_ntfs_mirrored_slave";
    case PartitionType::reserved_for_dr_dos: return "reserved_for_dr_dos";
    case PartitionType::reserved_for_dr_dos2: return "reserved_for_dr_dos2";
    case PartitionType::reserved_for_dr_dos3: return "reserved_for_dr_dos3";
    case PartitionType::corrupted_fat32_mirrored_slave: return "corrupted_fat32_mirrored_slave";
    case PartitionType::corrupted_fat32_mirrored_slave_2: return "corrupted_fat32_mirrored_slave_2";
    case PartitionType::ctos_memory_dump: return "ctos_memory_dump";
    case PartitionType::secured_fat16b: return "secured_fat16b";
    case PartitionType::secured_extented_partition_with_lba:
      return "secured_extented_partition_with_lba";
    case PartitionType::secured_fat_2: return "secured_fat_2";
    case PartitionType::secured_fat12_2: return "secured_fat12_2";
    case PartitionType::secured_fat16_2: return "secured_fat16_2";
    case PartitionType::secured_extented_partition_with_chs_2:
      return "secured_extented_partition_with_chs_2";
    case PartitionType::secured_fat16b_2: return "secured_fat16b_2";
    case PartitionType::cp_m_86: return "cp_m_86";
    case PartitionType::data_power_backup: return "data_power_backup";
    case PartitionType::boot_for_x86_supervisor_or_fat32_restore:
      return "boot_for_x86_supervisor_or_fat32_restore";
    case PartitionType::hidden_memory_dump: return "hidden_memory_dump";
    case PartitionType::fat16_utility_diagnostic: return "fat16_utility_diagnostic";
    case PartitionType::dg_ux_virtual_disk_manager_or_embrm:
      return "dg_ux_virtual_disk_manager_or_embrm";
    case PartitionType::st_avfs: return "st_avfs";
    case PartitionType::fat12_less16mb: return "fat12_less16mb";
    case PartitionType::read_only_fat12: return "read_only_fat12";
    case PartitionType::fat16_less32mb: return "fat16_less32mb";
    case PartitionType::logical_sectored_fat12_or_fat16_3:
      return "logical_sectored_fat12_or_fat16_3";
    case PartitionType::read_only_fat16: return "read_only_fat16";
    case PartitionType::linux_unified_key_setup: return "linux_unified_key_setup";
    case PartitionType::bfs: return "bfs";
    case PartitionType::skyfs: return "skyfs";
    case PartitionType::gpt_hybrid_mbr: return "gpt_hybrid_mbr";
    case PartitionType::gpt_protective_mbr: return "gpt_protective_mbr";
    case PartitionType::efi_system_partition: return "efi_system_partition";
    case PartitionType::linux_boot_loader: return "linux_boot_loader";
    case PartitionType::logical_sectored_fat12_or_fat16_4:
      return "logical_sectored_fat12_or_fat16_4";
    case PartitionType::fat16b_or_ngf_or_twinfs: return "fat16b_or_ngf_or_twinfs";
    case PartitionType::md0_md9_for_ngf_or_twinfs: return "md0_md9_for_ngf_or_twinfs";
    case PartitionType::read_only_fat16b: return "read_only_fat16b";
    case PartitionType::efat_or_ddr_drive: return "efat_or_ddr_drive";
    case PartitionType::linux_ext23_persistent_cache: return "linux_ext23_persistent_cache";
    case PartitionType::vmfs: return "vmfs";
    case PartitionType::vmware_swap_or_vmkcore: return "vmware_swap_or_vmkcore";
    case PartitionType::linux_raid_superblock: return "linux_raid_superblock";
    case PartitionType::old_linux_lvm_or_windows_admin_hidden:
      return "old_linux_lvm_or_windows_admin_hidden";
    case PartitionType::xenix_bad_block_table: return "xenix_bad_block_table";
  }
  ::std::abort();
}

}  // namespace mbr
