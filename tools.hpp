#pragma once

#include <optional>
#include <cstring>


template<class BytePOD, class SFINAE = std::enable_if_t<sizeof(BytePOD) == 1>>
inline
char first_hex(const BytePOD c) {
  static constexpr const std::array<char, 16> hexes{
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
  };
  return hexes[static_cast<uint8_t>(c) >> 4];
}

template<class BytePOD, class SFINAE = std::enable_if_t<sizeof(BytePOD) == 1>>
inline
char second_hex(const BytePOD c) {
  static constexpr const std::array<char, 16> hexes{
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
  };
  return hexes[static_cast<uint8_t>(c) & 0b1111];
}

template<class BytePOD, class SFINAE = std::enable_if_t<sizeof(BytePOD) == 1>>
inline
auto escape_nonascii(BytePOD const * const first, BytePOD const * const last) {
  std::string result;
  result.reserve(std::distance(first, last));
  for (auto curr = first; curr != last; ++curr) {
    if (::std::isprint(static_cast<unsigned char>(*curr))) {
      result.append(1, static_cast<char>(*curr));
    } else {
      result
        .append("\\x")
        .append(1, first_hex(*curr))
        .append(1, second_hex(*curr))
      ;
    }
  }
  return result;
}


template<class BytePOD, size_t ArraySize, class SFINAE = std::enable_if_t<sizeof(BytePOD) == 1>>
inline
auto hexdump(const std::array<BytePOD, ArraySize>& array)
    -> std::string
{
  auto result = std::string{};

  const auto find_newline_finish = [&array] (const auto newline_start) {
    return std::next(
      newline_start, std::min(int64_t{16}, std::distance(newline_start, array.end()))
    );
  };

  for ( auto newline_start = array.begin(),
        newline_finish = find_newline_finish(newline_start)
      ; newline_start != array.end()
      ; newline_start = newline_finish,
        newline_finish = find_newline_finish(newline_start)
  ) {
    const bool next_block_reached =
      (std::distance(array.begin(), newline_start) % 128) == 0 and array.begin() != newline_start;
    if (next_block_reached) {
      result.append(1, '\n');
    }

    const auto line_middle = std::next(
      newline_start, std::min(int64_t{8}, std::distance(newline_start, newline_finish))
    );

    for (auto current = newline_start; current != line_middle; ++current) {
      result
        .append(1, first_hex(*current))
        .append(1, second_hex(*current))
        .append(1, ' ')
      ;
    }
    if (line_middle != newline_finish) {
      result.append(1, ' ');
    }
    for (auto current = line_middle; current != newline_finish; ++current) {
      result
        .append(1, first_hex(*current))
        .append(1, second_hex(*current))
        .append(1, ' ')
      ;
    }

    result.pop_back();
    result.append(1, '\n');
  }
  if (not array.empty()) {
    result.pop_back();
  }
  return result;
}

// https://stackoverflow.com/questions/3418231/replace-part-of-a-string-with-another-string/3418285#3418285
inline
auto replace_all(std::string&& str, const std::string& from, const std::string& to)
    -> std::string
{
  if (from.empty()) {
    return str;
  }
  size_t start_pos = 0;
  while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
  }
  return str;
}


inline
auto separate_thousands(std::string&& str)
    -> std::string
{
  const intptr_t original_size = str.size();
  const intptr_t final_size = original_size + (original_size - 1) / 3;
  str.resize(final_size);
  for ( auto input_riter = std::prev(str.rend(), original_size), output_riter = str.rbegin()
      ; input_riter != output_riter
      ; std::advance(input_riter, 3), std::advance(output_riter, 4)
  ) {
    std::copy(input_riter, std::next(input_riter, 3), output_riter);
    *std::next(output_riter, 3) = '\'';
  }
  return str;
}
