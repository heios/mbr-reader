# mbr-reader

Library and console tool to read MasterBootRecord from supplied file.

## Example of work:
```yaml
$ ./build.sh
$ sudo head -c512 /dev/nvme0n1 > first_512_bytes.bin
$ ./mbr-reader first_512_bytes.bin
MBR:
  disk_id: 67ef5a11
  bootloader: >-
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00

    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00

    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00

    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
    00 00 00 00 00 00 00 00
  reserved_block: 00 00
  partition_tables:
    -
    partition_table:
      boot_indicator: not_bootable
      partition_type: gpt_protective_mbr
      CylinderHeadSector_span:
        first_sector: {cylinder: 0, head: 0, sector: 1}
        last_sector_inclusive: {cylinder: 1023, head: 254, sector: 255}
      LogicalBlockAddressing_span:
        first_sector_offset: 1
        last_sector_offset_exclusive: 1'000'215'216
        number_of_sectors: 1'000'215'215
        bytes_format:
          first_sector_offset: 512
          last_sector_offset_exclusive: 512'110'190'592
          number_of_bytes: 512'110'190'080
    -
    partition_table:
      boot_indicator: not_bootable
      partition_type: empty
      CylinderHeadSector_span:
        first_sector: {cylinder: 0, head: 0, sector: 0}
        last_sector_inclusive: {cylinder: 0, head: 0, sector: 0}
      LogicalBlockAddressing_span:
        first_sector_offset: 0
        last_sector_offset_exclusive: 0
        number_of_sectors: 0
        bytes_format:
          first_sector_offset: 0
          last_sector_offset_exclusive: 0
          number_of_bytes: 0
    -
    partition_table:
      boot_indicator: not_bootable
      partition_type: empty
      CylinderHeadSector_span:
        first_sector: {cylinder: 0, head: 0, sector: 0}
        last_sector_inclusive: {cylinder: 0, head: 0, sector: 0}
      LogicalBlockAddressing_span:
        first_sector_offset: 0
        last_sector_offset_exclusive: 0
        number_of_sectors: 0
        bytes_format:
          first_sector_offset: 0
          last_sector_offset_exclusive: 0
          number_of_bytes: 0
    -
    partition_table:
      boot_indicator: not_bootable
      partition_type: empty
      CylinderHeadSector_span:
        first_sector: {cylinder: 0, head: 0, sector: 0}
        last_sector_inclusive: {cylinder: 0, head: 0, sector: 0}
      LogicalBlockAddressing_span:
        first_sector_offset: 0
        last_sector_offset_exclusive: 0
        number_of_sectors: 0
        bytes_format:
          first_sector_offset: 0
          last_sector_offset_exclusive: 0
          number_of_bytes: 0
```


## Links

- https://wiki.osdev.org/MBR_(x86)
- https://en.wikipedia.org/wiki/Master_boot_record#PTE
