#pragma once

#include <array>
#include <cstdint>
#include <utility>
#include <optional>


namespace disk_addr
{

using LogicalBlockAddressing22 = uint32_t;
using LogicalBlockAddressing28 = uint32_t;
using LogicalBlockAddressing48 = uint64_t;

struct CylinderHeadSector24 {
  // https://en.wikipedia.org/wiki/Master_boot_record#cite_note-note-3-19
  uint8_t head;       // [0; 254]
  uint8_t sector;     // [1; 255]
  uint16_t cylinder;  // [0; 1023]
};

auto to_string(const CylinderHeadSector24) -> std::string;

namespace span
{
struct CHS24 {
  CylinderHeadSector24 first_sector;
  CylinderHeadSector24 last_sector_inclusive;
};

struct LBA28 {
  LogicalBlockAddressing28 first_sector;
  uint32_t number_of_sectors;
};
}  // namespace span

}  // namespace disk_addr


namespace mbr
{

static constexpr const int64_t bytes_in_sector = 512;

enum class BootIndicator : uint8_t;
enum class PartitionType : uint8_t;

struct PartitionTable {
  BootIndicator boot_indicator;
  PartitionType partition_type;
  ::disk_addr::span::CHS24 chs_span;
  ::disk_addr::span::LBA28 lba_span;
};

auto to_string(const PartitionTable) -> std::string;

// Structure `MasterBootRecord` is logical abstraction of MBR.
// Actual disk layout can be generated with function
//  `dump(const MasterBootRecord&) -> std::array<uint8_t, 512>`
struct Data {
  std::array<uint8_t, 440> bootloader_code;
  std::array<uint8_t, 4> disk_id;
  std::array<uint8_t, 2> reserved_block;
  std::array<PartitionTable, 4> partition_tables;
  static constexpr const std::array<uint8_t, 2> end_of_sector = {0b0101'0101, 0b1010'1010};
};

auto to_string(const Data&) -> std::string;


enum class ReadDataError {
  success = 0,
  bad_data_block_size,
  bad_partition_table_data,
  bad_end_of_sector,
};

auto to_string(const ReadDataError)
    -> std::string;

auto read_data(char const * const first, char const * const last)
    -> std::pair<Data, std::optional<ReadDataError>>;

auto read_partition_table(char const * const first, char const * const last)
    -> std::pair<PartitionTable, bool>;

}  // namespace mbr


// Details.
namespace mbr
{

enum class BootIndicator : uint8_t {
  not_bootable = 0,
  bootable = (uint8_t{1} << 7),
};

auto to_string(const BootIndicator) -> std::string;

enum class PartitionType : uint8_t {
  empty = 0x00,
  fat12_primary_or_logical_elsewhere = 0x01,
  xenix_root = 0x02,
  xenix_usr = 0x03,
  fat16 = 0x04,
  chs_8g = 0x05,
  fat16b = 0x06,
  ntfs_or_exfat = 0x07,
  logical_sectored_fat = 0x08,
  aix_qnx_os9 = 0x09,
  os2_boot = 0x0A,
  fat32_with_chs = 0x0B,
  fat32_with_lba = 0x0C,
  fat16b_with_lba = 0x0E,
  extended_with_lba = 0x0F,
  unisys_opus = 0x10,
  logical_or_hidden_fat12 = 0x11,
  hibernation_or_service_or_rescue = 0x12,
  logical_or_hidden_fat16 = 0x14,
  hidden_extended_with_chs = 0x15,
  hidden_fat16b = 0x16,
  hidden_ntfs_or_exfat = 0x17,
  ast_zero_suspended = 0x18,
  willowtech_photon_cos = 0x19,
  hidden_fat32 = 0x1B,
  hidden_fat32_with_lba = 0x1C,
  hidden_fat16_with_lba = 0x1E,
  hidden_extended_with_lba = 0x1F,
  willowsoft_overture_fs1 = 0x20,
  oxygen_file_system_fso2 = 0x21,
  oxygen_extended_partition_table = 0x22,
  windows_mobile_boot_xip = 0x23 ,
  logical_sectored_fat12_or_fat16 = 0x24,
  windows_mobile_imgfs = 0x25,
  microsoft_ibm_reserved_code26 = 0x26,
  hidden_ntfs_windows_recovery_env = 0x27,
  anthe_os_filesystem = 0x2A,
  syllable_secure = 0x2B,
  microsoft_ibm_reserved_code31 = 0x31,
  alien_internet_services_nos = 0x32,
  microsoft_ibm_reserved_code33 = 0x33,
  microsoft_ibm_reserved_code34 = 0x34,
  jfs = 0x35,
  microsoft_ibm_reserved_code36 = 0x36,
  theos_v3_2 = 0x38,
  plan9_edition3_partition = 0x39,
  theos_v4 = 0x3A,
  theos_v4_extended = 0x3B,
  partition_magic_in_progress = 0x3C,
  hidden_netware = 0x3D,
  pick_r83_or_venix_80286 = 0x40,
  risc_or_old_linux_or_powerpc = 0x41,
  sfs_or_old_linux_swap_or_microsoft_dynamic_extended = 0x42,
  old_linux_native = 0x43,
  goback_softwares = 0x44,
  priam_or_boot_us_manager = 0x45,
  eumel_elan_x46 = 0x46,
  eumel_elan_x47 = 0x47,
  eumel_elan_or_ergos_l3 = 0x48,
  aquila_or_alfs = 0x4A,
  aos_a2 = 0x4C,
  primary_qnx_posix = 0x4D,
  secondary_qnx_posix = 0x4E,
  tertiary_qnx_posix_or_oberon = 0x4F,
  lynx_rtos_or_oberon = 0x50,
  novell_or_on_track_aux1 = 0x51,
  cp_m_80 = 0x52,
  on_track_aux3 = 0x53,
  on_track_ddo = 0x54,
  ez_drive = 0x55,
  logical_sectored_fat12_or_fat16_2 = 0x56,
  drivepro_vndi = 0x57,
  priam_edisk_partitioned = 0x5C,
  hidden_fat12 = 0x61,
  unix_chs_or_hidden_fat12 = 0x63,
  netware_fs286_2_or_hidden_fat16 = 0x64,
  netware_fs386 = 0x65,
  netware_fs386_or_hidden_fat16 = 0x66,
  netware_wolf_mountain = 0x67,
  netware_code68 = 0x68,
  netware_code69 = 0x69,
  dragonfly_bsd_slice = 0x6C,
  dick_secure_multiboot = 0x70,
  microsoft_ibm_reserved_code71 = 0x71,
  apti_alternative_fat12 = 0x72,
  microsoft_ibm_reserved_code73 = 0x73,
  hidden_fat16b_2 = 0x74,
  ibm_pc_ix = 0x75,
  hidden_read_only_fat16b = 0x76,
  vndi_m2fs_m2cs = 0x77,
  xosl_bootloader = 0x78,
  apti_alternative_fat16_chs = 0x79,
  apti_alternative_fat16_lba = 0x7A,
  apti_alternative_fat16b_chs = 0x7B,
  apti_alternative_fat32_lba = 0x7C,
  apti_alternative_fat32_chs = 0x7D,
  primo_cache_level2 = 0x7E ,
  reserved_for_local_expirements = 0x7F,
  minix_fs_old = 0x80,
  minix_fs = 0x81,
  linux_swap_or_solaris_x86 = 0x82,
  native_linux_fs = 0x83,
  hibernation_or_hidden_fat16 = 0x84,
  linux_extented = 0x85,
  linux_old_raid_superblock_or_fat16b_mirrored = 0x86,
  mirrored_hpfs_ntfs = 0x87,
  linux_plaintext_partition_table = 0x88,
  linux_kernel_image = 0x8A,
  mirrored_fat32 = 0x8B,
  legacy_mirrored_fat32 = 0x8C,
  hidden_fat12_code8d = 0x8D,
  linux_lvm = 0x8E,
  hidden_fat16 = 0x90,
  hidden_extended_with_chs_code91 = 0x91,
  hidden_fat16b_3 = 0x92,
  hidden_linux_or_amoeba_native = 0x93,
  amoeba_back_block_table = 0x94,
  exopc_native = 0x95,
  iso9660 = 0x96,
  hidden_fat32_code97 = 0x97,
  hidden_fat32_or_service_partition = 0x98,
  early_unix = 0x99,
  hidden_fat16_2 = 0x9A,
  hidden_extended_with_lba_2 = 0x9B,
  forthos = 0x9E,
  bsd_os_3_0_plus = 0x9F,
  hp_diagnostic_or_hibernate = 0xA0,
  hp_expansion_or_hibernate = 0xA1,
  altera_hps_arm_preloader = 0xA2,
  hp_expansion = 0xA3,
  hp_expansion_2 = 0xA4,
  bsd_slice = 0xA5,
  openbsd_slice = 0xA6,
  nextstep = 0xA7,
  mac_osx_ufs = 0xA8,
  netbsd_slice = 0xA9,
  msdos_fat12 = 0xAA,
  mac_osx_boot = 0xAB,
  max_osx_raid = 0xAC,
  risc_os_adfs = 0xAD,
  shagos = 0xAE,
  hfs_maybe_plus = 0xAF,
  star_tools_boot = 0xB0,
  qnx_power_safe = 0xB1,
  qnx_power_safe2 = 0xB2,
  qnx_power_safe3 = 0xB3,
  hp_expansion_3 = 0xB4,
  hp_expansion_or_corrupted_fat16b = 0xB6,
  corrupted_hpfs_ntfs_mirrored_master = 0xB7,
  bsdi_swap_or_native = 0xB8,
  corrupted_fat32_mirrored_master = 0xBB,
  corrupted_fat32_mirrored_master_or_backup = 0xBC,
  bonny_dos_286 = 0xBD,
  solaris8_boot = 0xBE,
  solaris_x86 = 0xBF,
  secured_fat = 0xC0,
  secured_fat12 = 0xC1,
  hidden_linux_native = 0xC2,
  hidden_linux_swap = 0xC3,
  secured_fat16 = 0xC4,
  secured_extented_partition_with_chs = 0xC5,
  corrupted_fat16b_mirrored_slave = 0xC6,
  corrupted_hpfs_ntfs_mirrored_slave = 0xC7,
  reserved_for_dr_dos = 0xC8,
  reserved_for_dr_dos2 = 0xC9,
  reserved_for_dr_dos3 = 0xCA,
  corrupted_fat32_mirrored_slave = 0xCB,
  corrupted_fat32_mirrored_slave_2 = 0xCC,
  ctos_memory_dump = 0xCD,
  secured_fat16b = 0xCE,
  secured_extented_partition_with_lba = 0xCF,
  secured_fat_2 = 0xD0,
  secured_fat12_2 = 0xD1,
  secured_fat16_2 = 0xD4,
  secured_extented_partition_with_chs_2 = 0xD5,
  secured_fat16b_2 = 0xD6,
  cp_m_86 = 0xD8,
  data_power_backup = 0xDA,
  boot_for_x86_supervisor_or_fat32_restore = 0xDB,
  hidden_memory_dump = 0xDD,
  fat16_utility_diagnostic = 0xDE,
  dg_ux_virtual_disk_manager_or_embrm = 0xDF,
  st_avfs = 0xE0,
  fat12_less16mb = 0xE1,
  read_only_fat12 = 0xE3,
  fat16_less32mb = 0xE4,
  logical_sectored_fat12_or_fat16_3 = 0xE5,
  read_only_fat16 = 0xE6,
  linux_unified_key_setup = 0xE8,
  bfs = 0xEB,
  skyfs = 0xEC,
  gpt_hybrid_mbr = 0xED,
  gpt_protective_mbr = 0xEE,
  efi_system_partition = 0xEF,
  linux_boot_loader = 0xF0,
  logical_sectored_fat12_or_fat16_4 = 0xF2,
  fat16b_or_ngf_or_twinfs = 0xF4,
  md0_md9_for_ngf_or_twinfs = 0xF5,
  read_only_fat16b = 0xF6,
  efat_or_ddr_drive = 0xF7,
  linux_ext23_persistent_cache = 0xF9,
  vmfs = 0xFB,
  vmware_swap_or_vmkcore = 0xFC,
  linux_raid_superblock = 0xFD,
  old_linux_lvm_or_windows_admin_hidden = 0xFE ,
  xenix_bad_block_table = 0xFF,
};


auto to_string(const PartitionType) -> std::string;

}  // namespace mbr
